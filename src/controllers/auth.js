import {Users} from "../models/";
import bcrypt from "bcryptjs";
import {generateJWT} from "../middlewares/jwt";

//1. Completar la logica para manejar el inicio de sesión
// - responder con un codigo de estado 401 cuando las credenciales sean incorrectas
// - responder con un mensaje (message) y codigo de estado 200 cuando las credenciales sean correctas
// - responder con el token jwt (token) 
export const login = async (req, res) => {
    const {email, password} = req.body;
    //Solicitar el registro del usuario que tenga el email solicitado
    try{
        let results = await Users.findOne({where: {email}});
        //Comparar la contraseña
        let validPassword = bcrypt.compareSync(password, results.password);
        if(validPassword){
            //Generar la contraseña
            let token = generateJWT({
                id: results.id,
                firstName: results.firstName,
                lastName: results.lastName,
                email: email
            });
            
            res.json({
                message: "Has iniciado sesión correctamente",
                token
            });
        }else{
            //Enviaremos un error
            res.status(401).json({
                message: "Las credenciales son incorrectas"
            });
        }
    }catch(error){
        res.status(401).json({
            message: "Las credenciales son incorrectas"
        });
    }
}

//2. Completar el registro de usuario
// - responder con un codigo de estado fallido 400 > cuando hagan falta campos o cuando el usuario ya exista en la base de datos
// - responder con el objeto del usuario que ha sido creado y un codigo 201 cuando el registro sea satisfactorio
export const signUp = async (req, res) => {
    
}

