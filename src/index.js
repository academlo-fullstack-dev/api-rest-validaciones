import express from "express";
import authRouter from "./routes/auth";
import usersRouter from "./routes/users";
import sendEmail from "./utils/nodemailer";
import {validateJWT} from "./middlewares/jwt";

const app = express();

app.use(express.json());

app.use(authRouter);
app.use("/api/v1", validateJWT, usersRouter);

app.post("/send-email", (req, res) => {
    try{
        sendEmail();
        res.json({
            message: "El correo ha sido enviado satisfactoriamente"
        });
    }catch(error){
        console.log(error);
    }
});

export default app;