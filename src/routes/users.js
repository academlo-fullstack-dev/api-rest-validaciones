import express from "express";
import {getAll, create} from "../controllers/users";
import {validate, userSchema} from "../middlewares/validators";

const router = express.Router();

router.get("/users", getAll);
router.post("/users", validate(userSchema), create);

export default router;