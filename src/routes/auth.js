import express from "express";
import {login, signUp} from "../controllers/auth";
const router = express.Router();

router.post("/login", login);
router.post("/signin", signUp);

export default router;